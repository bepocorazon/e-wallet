<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserAccountController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'App\Http\Controllers\Api\UserController@login');
Route::post('register', 'App\Http\Controllers\Api\UserController@register');

Route::group(['prefix'=>'user','middleware' => 'auth:api'], function(){
    Route::get('/list', 'App\Http\Controllers\Api\UserController@list');
    Route::get('/detail', 'App\Http\Controllers\Api\UserController@details');
    Route::post('/logout', 'App\Http\Controllers\Api\UserController@logout');
    Route::resource('/account', UserAccountController::class);
}); 

Route::group(['prefix'=>'wallet','middleware' => 'auth:api'], function(){
    Route::get('/balance', 'App\Http\Controllers\Api\WalletController@balance');
    Route::get('/mutation', 'App\Http\Controllers\Api\WalletController@mutation');
    Route::post('/topup', 'App\Http\Controllers\Api\WalletController@topup');
    Route::post('/withdraw', 'App\Http\Controllers\Api\WalletController@withdraw');
    Route::post('/transfer', 'App\Http\Controllers\Api\WalletController@transfer');
}); 
