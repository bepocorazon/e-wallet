<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserAccount;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserAccountController extends Controller
{

    public $successStatus = 200;
    
    public function index()
    {
        $userID = json_decode(Auth::user()->id);
        $userAccount = UserAccount::join('users', 'user_accounts.user_id', '=', 'users.id')->where('user_id', $userID)->get(['user_accounts.*', 'users.name as user_name']);

        return response()->json([
            "success" => true,
            "data" => $userAccount
        ], $this->successStatus);
    }
    
    public function store(Request $request)
    {
        $userID = json_decode(Auth::user()->id);

        $validator = Validator::make($request->all(), [
            'bank' => 'required',
            'on_behalf_name' => 'required',
            'account_number' => 'required|numeric'
        ]);
        
        if ($validator->fails() ){
            return response()->json(['error'=>$validator->errors()], 401);      
        }

        $input = $request->all();
        $input['user_id'] = $userID;
        
        $userAccount = UserAccount::create($input);
        return response()->json([
            "success" => true,
            "message" => "User Account created successfully.",
            "data" => $userAccount
        ]);
    }

    public function show($id)
    {
        $userAccount = UserAccount::find($id);
        if (is_null($userAccount)) {
            return response()->json(['error'=>'User Account not found.'], 401);    
        }
        return response()->json([
            "success" => true,
            "data" => $userAccount
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'bank' => 'required',
            'on_behalf_name' => 'required',
            'account_number' => 'required|numeric'
        ]);
        
        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);           
        }
        
        $userAccount = UserAccount::find($id);
        $input = $request->all();

        $userAccount->bank = $input['bank'];
        $userAccount->on_behalf_name = $input['on_behalf_name'];
        $userAccount->account_number = $input['account_number'];
        $userAccount->save();
        
        return response()->json([
            "success" => true,
            "message" => "User Account updated successfully.",
            "data" => $userAccount
        ]);
    }

    public function destroy($id)
    {
        $userAccount = UserAccount::find($id);
        $userAccount->delete();
        return response()->json([
            "success" => true,
            "message" => "User Account deleted successfully.",
            "data" => $userAccount
        ]);
    }
}
