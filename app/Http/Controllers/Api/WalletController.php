<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\Wallet;
use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;
use Validator;

class WalletController extends Controller
{

    public $successStatus = 200;

    public function balance()
    {
        $userID = json_decode(Auth::user()->id);
        $userBalance = Wallet::join('users', 'wallets.user_id', '=', 'users.id')->where('user_id', $userID)->get(['wallets.*', 'users.name as user_name']);

        return response()->json([
            "success" => true,
            "data" => $userBalance
        ], $this->successStatus);
    }

    public function mutation()
    {
        $userID = json_decode(Auth::user()->id);
        $userTransfer = Transaction::join('users', 'transactions.user_id', '=', 'users.id')->where('user_id', $userID)->get(['transactions.*', 'users.name as user_name']);

        return response()->json([
            "success" => true,
            "data" => $userTransfer
        ], $this->successStatus);
    }

    public function topup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input = $request->all();
        $userID = json_decode(Auth::user()->id);
        
        try {
            $inputTransaction['user_id'] = $userID;
            $inputTransaction['ref_number'] = md5(time());
            $inputTransaction['type'] = "TOPUP";
            $inputTransaction['description'] = "top up balance " . $input['amount'];
            $inputTransaction['amount'] = $input['amount'];
            $userTransaction = Transaction::create($inputTransaction);

            $userWallet = Wallet::find($userID);
            $userWallet->balance = $userWallet->balance + $input['amount'];
            $userWallet->save();
        } catch (\Illuminate\Database\QueryException $e) {
            $errorCode = $e->errorInfo[1];
            if ($errorCode == '1062') {
                return response()->json(['message'=>'Duplicate Entry'], 403);
            } elseif ($errorCode == '1064') {
                return response()->json(['message'=>'Some fields are required'], 422);
            }
        }

        return response()->json([
            "success" => true,
            "message" => "User Top Up successfully.",
        ]);
    }

    public function withdraw(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input = $request->all();
        $userID = json_decode(Auth::user()->id);
        $userAccount = UserAccount::find($input['user_account_id']);
        
        try {
            $inputTransaction['user_id'] = $userID;
            $inputTransaction['ref_number'] = md5(time());
            $inputTransaction['type'] = "WITHDRAW";
            $inputTransaction['description'] = "withdrawal balance to " . $userAccount->account_number . " " . $userAccount->bank . " account, on behalf name " . $userAccount->on_behalf_name . ", amount " . $input['amount'];
            $inputTransaction['amount'] = $input['amount'];
            $userTransaction = Transaction::create($inputTransaction);

            $userWallet = Wallet::find($userID);
            $userWallet->balance = $userWallet->balance - $input['amount'];
            $userWallet->save();
        } catch (\Illuminate\Database\QueryException $e) {
            $errorCode = $e->errorInfo[1];
            if ($errorCode == '1062') {
                return response()->json(['message'=>'Duplicate Entry'], 403);
            } elseif ($errorCode == '1064') {
                return response()->json(['message'=>'Some fields are required'], 422);
            }
        }

        return response()->json([
            "success" => true,
            "message" => "User Withdraw successfully.",
        ]);
    }

    public function transfer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input = $request->all();
        $userID = json_decode(Auth::user()->id);
        $userWalletFrom = Wallet::join('users', 'wallets.user_id', '=', 'users.id')->find($userID, '*');
        $userWalletTo = Wallet::join('users', 'wallets.user_id', '=', 'users.id')->find($input['user_id'], '*');
        
        try {
            $inputTransaction['user_id'] = $userID;
            $inputTransaction['ref_number'] = md5(time());
            $inputTransaction['type'] = "TRANSFER";
            $inputTransaction['description'] = "transfer balance from " . $userWalletFrom->name . " to " . $userWalletTo->name . " amount " . $input['amount'];
            $inputTransaction['amount'] = $input['amount'];
            $userTransaction = Transaction::create($inputTransaction);
            
            $inputTransactionRecieved['user_id'] = $input['user_id'];
            $inputTransactionRecieved['ref_number'] = md5(time() + 1);
            $inputTransactionRecieved['type'] = "RECIEVED TRANSFER";
            $inputTransactionRecieved['description'] = "recieved transfer balance from " . $userWalletTo->name . " to " . $userWalletFrom->name . " amount " . $input['amount'];
            $inputTransactionRecieved['amount'] = $input['amount'];
            $userTransactionRecieved = Transaction::create($inputTransactionRecieved);

            $userWalletFrom->balance = $userWalletFrom->balance - $input['amount'];
            $userWalletFrom->save();

            $userWalletTo->balance = $userWalletTo->balance + $input['amount'];
            $userWalletTo->save();
        } catch (\Illuminate\Database\QueryException $e) {
            $errorCode = $e->errorInfo[1];
            if ($errorCode == '1062') {
                return response()->json(['message'=>'Duplicate Entry'], 403);
            } elseif ($errorCode == '1064') {
                return response()->json(['message'=>'Some fields are required'], 422);
            }
        }

        return response()->json([
            "success" => true,
            "message" => "User Transfered successfully.",
        ]);
    }

    
} 